package com.edificio.PID_202201_Grupo5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pid202201Grupo5Application {

	public static void main(String[] args) {
		SpringApplication.run(Pid202201Grupo5Application.class, args);
	}

}
