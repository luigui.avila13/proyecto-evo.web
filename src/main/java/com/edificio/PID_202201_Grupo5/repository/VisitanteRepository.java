package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edificio.PID_202201_Grupo5.entity.Visitante;

public interface VisitanteRepository extends JpaRepository<Visitante, Integer>{
	
	public abstract List<Visitante> findAllByEstado(int estado);
	
	// Consulta por DNI y Sexo
	@Query("select v from Visitante v where ( ?1 is '' or v.dni = ?1 ) and ( ?2 is -1 or v.sexoVisitante = ?2) and v.estado = ?3")
	public abstract List<Visitante> listaVisitantePorDNIandSexo(String dni, int sexo, int estado);
	
	public abstract Visitante findByDni(String dni);
	

}