package com.edificio.PID_202201_Grupo5.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edificio.PID_202201_Grupo5.entity.TipoServicio;

public interface TipoServicioRepository extends JpaRepository<TipoServicio, Integer>{

}
