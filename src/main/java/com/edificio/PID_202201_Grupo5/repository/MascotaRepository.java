package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edificio.PID_202201_Grupo5.entity.Mascota;


public interface MascotaRepository extends JpaRepository<Mascota, Integer> {
	
	
	public abstract List<Mascota> findAllByEstado(int estado);
	
	@Query("select m from Mascota m where (?1 is -1 or m.departamentoMascota.iddepartamento = ?1) and (?2 is -1 or m.especie.idespecie= ?2)")
	public abstract List<Mascota> listarporparametros(int idDepartamento, int especie);
	
}
