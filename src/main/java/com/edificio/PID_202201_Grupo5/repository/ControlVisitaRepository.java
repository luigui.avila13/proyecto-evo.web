package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edificio.PID_202201_Grupo5.entity.ControlVisita;

public interface ControlVisitaRepository extends JpaRepository<ControlVisita, Integer> {
	

	public abstract List<ControlVisita> findAllByEstado (int estado);
	
	@Query("select v from ControlVisita v where (?1 is '' or v.visitanteVisita.nombres like ?1) and (?2 is '' or v.visitanteVisita.dni = ?2) and v.estado=?3 ")
	public List<ControlVisita> listacontolvisitaporparametros (String nombre, String dni, int estado);
	
	
	



}
