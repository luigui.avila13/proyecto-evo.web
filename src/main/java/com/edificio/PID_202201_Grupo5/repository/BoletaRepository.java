package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edificio.PID_202201_Grupo5.entity.BoletaPorServicio;

public interface BoletaRepository extends JpaRepository<BoletaPorServicio, Integer>{

	@Query("select b from  BoletaPorServicio b where (?1 is -1 or b.boletaDepartamento.nrodepartamento = ?1) and (?2 is '' or b.mes = ?2) "
			+ "and (?3 is -1 or b.servicio.idtiposervicio = ?3) and (?4 is -1 or b.estadoBoleta.idestadoboleta = ?4)")
	public List<BoletaPorServicio> listarBoletasPorParametros(int nroDepa,String mes, int servicio, int estado);
}
