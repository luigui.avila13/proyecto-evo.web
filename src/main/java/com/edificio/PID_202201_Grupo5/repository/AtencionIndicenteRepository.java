package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Incidente;

public interface AtencionIndicenteRepository extends JpaRepository<Incidente, Integer> {
	
	
	@Query("select i from Incidente i where (?1 is -1 or i.incidenteDepartamento.iddepartamento = ?1) and (?2 is -1 or i.incidente.idtipoincidente = ?2) and (?3 is -1 or i.estadoIncidente = ?3)")
	public List<Incidente> listaincidenteporparametros( int departamento, int tipoincidente, int estado );
	
	
;
	

}
