package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edificio.PID_202201_Grupo5.entity.Edificio;

public interface edificioRepository extends JpaRepository<Edificio, Integer>{

	public abstract List<Edificio> findAllByEstado(int estado);
}
