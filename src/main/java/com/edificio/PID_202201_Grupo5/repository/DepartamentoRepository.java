package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import javax.websocket.Session;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edificio.PID_202201_Grupo5.entity.Departamento;


public interface DepartamentoRepository extends JpaRepository<Departamento, Integer>{
	
	public abstract List<Departamento> findAllByEstado(int estado);

	@Query("select d from Departamento d where (?1 is -1 or d.edificio.idedificio = ?1)")
	public List<Departamento> listarPorEdificio(int idEdificio);
	
	
	@Query("select d from Departamento d where (?1 is -1 or d.edificio.idedificio = ?1) and (?2 is -1 or d.tipoDepartamento.idtipo = ?2)")
	public List<Departamento> listarporparametros(int idEdificio, int idTipo);

}
	


