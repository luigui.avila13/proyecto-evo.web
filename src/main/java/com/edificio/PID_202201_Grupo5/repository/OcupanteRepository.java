package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;

import com.edificio.PID_202201_Grupo5.entity.Ocupantes;
import com.edificio.PID_202201_Grupo5.entity.PropietarioDep;

import org.springframework.data.jpa.repository.Query;

public interface OcupanteRepository extends JpaRepository<Ocupantes, Integer>{
	
	public abstract List<Ocupantes> findAllByEstado(int estado);

	@Query("select d from Ocupantes d where (?1 is -1 or d.departamentoOcupante.iddepartamento = ?1) and (?2 is -1 or d.sexoOcupante.idsexo = ?2) and (?3 is '%%' or d.dni = ?3) and (d.estado = 1)")
	public List<Ocupantes> listarPorParametros(int idDepartamento, int idSexo, String dni);
	


}
