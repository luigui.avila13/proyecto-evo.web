package com.edificio.PID_202201_Grupo5.repository;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edificio.PID_202201_Grupo5.entity.PropietarioDep;

public interface PropietarioRepository extends JpaRepository<PropietarioDep, Integer>{
	
	public abstract List<PropietarioDep> findAllByEstado(int estado);
	
	@Query("select p from PropietarioDep p where (?1 is '' or p.nombres like ?1) and (?2 is '' or p.dni =?2) and p.estado =1")
	public List<PropietarioDep> listarPorParametros(String nombres, String dni);

	@Query("select p from PropietarioDep p where (?1 is -1 or p.departamentoPropietario.iddepartamento = ?1)")
	public List<PropietarioDep> listarPorDepartamento(int idDepartamento);
	
	@Query("select p from PropietarioDep p where (?1 is -1 or p.edificioPropietario.idedificio = ?1)")
	public List<PropietarioDep> listarPorEdificio(int idEdificio);

}
	