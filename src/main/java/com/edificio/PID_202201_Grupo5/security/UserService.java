package com.edificio.PID_202201_Grupo5.security;


import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;






@Service
public class UserService implements UserDetailsService {

	@Autowired
	UsuarioService usuarioService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	
		Usuario user;
		user = usuarioService.buscarPorUser(username);
		
		if(user == null) {
			throw new UsernameNotFoundException("Usuario no encontrado");
		}
		
		return new MyUserDetails(user);
	}

}
