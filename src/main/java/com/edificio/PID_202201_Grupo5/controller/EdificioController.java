package com.edificio.PID_202201_Grupo5.controller;

import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

@Controller
@RequestMapping("/edificio")
public class EdificioController {

	@Autowired
	private EdificioService eService;
	@Autowired
	private UsuarioService uService;
	
	@RequestMapping("/")
	public String index(Model model, HttpSession session) {
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = uService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());
			model.addAttribute("edificios", eService.listarActivos(1));
			model.addAttribute("edificiosI", eService.listarActivos(0));
		return "edificio";
	}
	
	
	@RequestMapping("/guardar")
	public String guardar(@RequestParam("idedificio") String cod ,@RequestParam("denominacion") String denominacion, @RequestParam("nrodepisos") String pisos, RedirectAttributes redirect) {
		
		try {
			
			if(cod.isEmpty() || denominacion.isEmpty() || pisos.isEmpty()) {
				redirect.addFlashAttribute("error", "Imposible guardar con datos en blanco");
			}else if(!cod.trim().matches("[0-9]{1,}") || !denominacion.trim().matches("[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9\\s]{4,255}") ||
					!pisos.trim().matches("[0-9]{1,2}")) {
				redirect.addFlashAttribute("error","Error: Los datos no cumplen el formato establecido");
			}else {
				int codigo = Integer.parseInt(cod);
				int nroPisos = Integer.parseInt(pisos);
				
				Edificio bean = new Edificio();
				bean.setDenominacion(denominacion);
				bean.setNrodepisos(nroPisos);
				bean.setFecharegistro(new Date());
				bean.setEstado(1);
				if(codigo != 0) {
					bean.setIdedificio(codigo);
					eService.actualizarEdificio(bean);
					redirect.addFlashAttribute("MENSAJE", "Edificio actualizado");
				}else {
					eService.registrarEdificio(bean);
					redirect.addFlashAttribute("MENSAJE", "Edificio registrado");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("error", "No se pudo guardar");
		}
		
		return "redirect:/edificio/";
	}
	
	@RequestMapping("/eliminar")
	public String eliminar(@RequestParam("idEdificio") String cod, RedirectAttributes redirect) {
		
		try {
			
			if(cod.isEmpty()) {
				redirect.addFlashAttribute("error", "Error: Identificador en blanco");
			}else if(!cod.trim().matches("[0-9]{1,}")) {
				redirect.addFlashAttribute("error","Error: Se esperaba el id numerico");
			}else {
				int codi = Integer.parseInt(cod);
				Edificio edificio = eService.buscarPorCodigo(codi);
				edificio.setEstado(0);
				eService.actualizarEdificio(edificio);
				redirect.addFlashAttribute("MENSAJE", "Edificio eliminado");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("error","Ocurrio un error al eliminar");
		}
		
		return "redirect:/edificio/";
	}
	
	
	
	@RequestMapping("/buscar")
	@ResponseBody
	public Edificio buscar(@RequestParam("idEdificio") String cod, RedirectAttributes redirect) {
		Edificio e = null;
		try {
			if(cod.trim().matches("[0-9]{1,10}")) {
				int codigo = Integer.parseInt(cod);
				e = eService.buscarPorCodigo(codigo);
			}else {
				redirect.addFlashAttribute("MENSAJE", "Se esperaba el id");
			}
			
		} catch (Exception e2) {
			e2.printStackTrace();
			redirect.addFlashAttribute("MENSAJE", "Opps! Ocurrio un error");	
		}
		return e;
	}
	
	
	@RequestMapping("/activar")
	public String activarEdificio(@RequestParam("idEdificio") int id, RedirectAttributes redirect) {
		
		try {
			
			Edificio bean = eService.buscarPorCodigo(id);
			bean.setEstado(1);
			eService.actualizarEdificio(bean);
			redirect.addFlashAttribute("MENSAJE","Edificio activado correctamente");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("error", "Ocurrio un error al activar");
		}
		
		return "redirect:/edificio/";
	}
	
	
	
	
}
