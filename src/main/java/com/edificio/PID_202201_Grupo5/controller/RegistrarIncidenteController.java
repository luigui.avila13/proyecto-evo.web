package com.edificio.PID_202201_Grupo5.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.print.attribute.standard.Sides;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.EstadoIncidente;
import com.edificio.PID_202201_Grupo5.entity.Incidente;
import com.edificio.PID_202201_Grupo5.entity.TipoDepartamento;
import com.edificio.PID_202201_Grupo5.entity.TipoIncidente;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.EstadoIncidenteService;
import com.edificio.PID_202201_Grupo5.service.IncidenteService;
import com.edificio.PID_202201_Grupo5.service.TipoDepartamentoService;
import com.edificio.PID_202201_Grupo5.service.TipoIncidenteService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

@Controller
@RequestMapping("/registrarIncidente")
public class RegistrarIncidenteController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private IncidenteService incidenteService;
	
	@Autowired
	private EdificioService edificioService;
		
	@Autowired
	private DepartamentoService departamentoService;
	
	@Autowired
	private EstadoIncidenteService estadoIncidenteService;
	
	@Autowired
	private TipoIncidenteService tipoIncidenteService;

	@RequestMapping("/")
	private String index(Model model, HttpSession session) {
		
		//model.addAttribute("estados", estadoIncidenteService.listarEstadoIncidentes());
		model.addAttribute("tipos", tipoIncidenteService.listarTipoIncidentes());
		model.addAttribute("edificios", edificioService.listadoEdificio());
		model.addAttribute("departamentos", departamentoService.listadoDepartamentos());
		
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());
		return "registrarIncidente";
	}

	@RequestMapping("/listadepartamentoporedificio")
	@ResponseBody
	public List<Departamento> listaPorEdificio(
			@RequestParam(name = "idEdificio", required = false, defaultValue = "") int idEdificio){
		List<Departamento> lista=null;
		try {
			lista=departamentoService.listarPorEdificio(idEdificio);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	@RequestMapping(value = "/registrar")
	public String registrar(@RequestParam("idIncidente") int id, @RequestParam("departamento") int dep,
							@RequestParam("tipoIncidente") int tipoInc,
							@RequestParam("comentario") String comentario, 
							@RequestParam("fechaIncidente") String fecha, 
							RedirectAttributes redirect, HttpSession session) {
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Usuario u = (Usuario) session.getAttribute("usuario");
			Incidente bean = new Incidente();
			bean.setIncidenteDepartamento(new Departamento(dep));
			bean.setUsuarioIncidente(new Usuario(u.getIdusuario()));
			bean.setIncidente(new TipoIncidente(tipoInc));
			bean.setFecharegistro(new Date());
			bean.setEstadoIncidente(new EstadoIncidente(2));
			bean.setComentario(comentario);
			bean.setFechaincidente(df.parse(fecha));
			if(id==0) {
				incidenteService.registrar(bean);
				redirect.addFlashAttribute("MENSAJE","Incidente registrado correctamete");
			}
		} catch (Exception e) {
			redirect.addFlashAttribute("ERROR","Error al guardar.");
			e.printStackTrace();
		}
		return "redirect:/registrarIncidente/";
	}
	
}
