package com.edificio.PID_202201_Grupo5.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.BoletaPorServicio;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.EstadoBoleta;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.BoletaService;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.EstadoBoletaService;
import com.edificio.PID_202201_Grupo5.service.TipoServicioService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

@Controller
@RequestMapping("/pagarBoleta")
public class PagoBoletaController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private BoletaService boletaService;
	
	@Autowired
	private TipoServicioService tipoServicioService;
	
	@Autowired
	private EstadoBoletaService estadoBoletaService;
	
	@Autowired
	private DepartamentoService departamentoService;
	
	@RequestMapping("/")
	private String index(HttpSession session, Model model) {
		
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
		for (var a : lista) {
			Enlace e = new Enlace();
			e.setIdenlace(a.getIdenlace());
			e.setDescripcionEnlace(a.getDescripcionEnlace());
			e.setRuta("../" + a.getRuta());
			e.setListaRolEnlace(null);
			lista2.add(e);
		}
		model.addAttribute("menus", lista2);
		model.addAttribute("fullNameUser", u.getNombres() + " " + u.getApepaterno());
		
		model.addAttribute("tipoServicio", tipoServicioService.listarTodos());
		model.addAttribute("estadoBoleta", estadoBoletaService.listarTodos());
		model.addAttribute("departamentos", departamentoService.listarporestado(1));
		
		
		return "pagoBoleta" ;
	}
	
	
	@RequestMapping("/filtrarBoletas")
	@ResponseBody
	public List<BoletaPorServicio> filtrar(
			@RequestParam(name = "nroDepa", required = false, defaultValue = "-1") int nroDepa,
			@RequestParam(name = "mes", required = false, defaultValue = "") String mes,
			@RequestParam(name = "servicio", required = false, defaultValue = "-1") int idServicio,
			@RequestParam(name = "estado", required = false, defaultValue = "-1") int idEstado){
		
		List<BoletaPorServicio> lista = null;
		try {
			lista = boletaService.filtroPorParametrosBoleta(nroDepa, mes, idServicio, idEstado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	@RequestMapping("/procesarPago")
	public String pagarBoleta(@RequestParam("idBoleta") int cod, RedirectAttributes redirect) {
		 
		try {
			
			BoletaPorServicio b = boletaService.buscarPorId(cod);
			if(b == null) {
				redirect.addFlashAttribute("error", "No se pudo realizar el pago. Id inexistente");
			}else {
				b.setEstadoBoleta(new EstadoBoleta(1));
				b.setFechapago(new Date()); //date de java util
				boletaService.procesoDePago(b);
				redirect.addFlashAttribute("MENSAJE", "La operación de pago fue exitosa");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("error", "No se puedo realizar la operación");
		}
		
		
		return "redirect:/pagarBoleta/";
	}
	
	
	@RequestMapping("/buscar")
	@ResponseBody
	public BoletaPorServicio buscar(@RequestParam("idBoleta") int cod, RedirectAttributes redirect) {
		BoletaPorServicio e = null;
		try {
				
			e = boletaService.buscarPorId(cod);
			
		} catch (Exception e2) {
			e2.printStackTrace();	
		}
		return e;
	}


	
}
