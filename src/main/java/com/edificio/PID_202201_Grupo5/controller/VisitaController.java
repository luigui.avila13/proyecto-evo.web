package com.edificio.PID_202201_Grupo5.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;

import com.edificio.PID_202201_Grupo5.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.ControlVisitaService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;
import com.edificio.PID_202201_Grupo5.service.VisitanteService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/visita")
public class VisitaController {

	@Autowired
	private ControlVisitaService visitaservice;

	@Autowired
	private EdificioService edificioService;

	@Autowired
	private DepartamentoService departamentoService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private VisitanteService visitanteService;



	@RequestMapping("/")
	private String index(Model model, HttpSession session) {


		model.addAttribute("edificio", edificioService.listarActivos(1));
		model.addAttribute("visitassalio", visitaservice.listarporestado(0));
		model.addAttribute("visitas", visitaservice.listarporestado(1));

		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());


		return "controlVisita";
	}
	
	@RequestMapping("/RegistraVisitante")
	public String index() {
		return "redirect:/visitante/";
	}

	@RequestMapping("/listadepartamentoporedificio")
	@ResponseBody
	public List<Departamento> listaPorEdificio(
			@RequestParam(name = "idEdificio", required = false, defaultValue = "") int idEdificio){
		List<Departamento> lista=null;
		try {
			lista=departamentoService.listarPorEdificio(idEdificio);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}


	@RequestMapping(value = "/registrar")
	public String registrar(@RequestParam("idvisita") int id,
							@RequestParam("visitante") int vis,
							@RequestParam("departamento") int dep,
							@RequestParam("ingreso") String ing,
							//@RequestParam("salida") String sal,
							//@RequestParam("estado") int est,
							//@RequestParam("comentario") String com,
							@RequestParam("visitanteDni") String dni,
							RedirectAttributes redirect){
		try {
			ControlVisita bean=new ControlVisita();
			bean.setVisitanteVisita(new Visitante(vis));
			bean.setDepartamentoVisita(new Departamento(dep));
			//bean.setIngreso(ing);
			bean.setIngreso(new Date());
			bean.setSalida(null);
			bean.setEstado(1);
			bean.setComentario(null);
			bean.setFecharegistro(new Date());

			List<ControlVisita> lista=null;
			lista=visitaservice.listacontolvisitaporparametros("%%", dni,1);

			if(lista.isEmpty()){
				if(id!=0) {
					bean.setIdvisita(id);
					visitaservice.registrarControlVisita(bean);
					redirect.addFlashAttribute("MENSAJE","Visita actualizada correctamente");
				}else{
					visitaservice.registrarControlVisita(bean);
					redirect.addFlashAttribute("MENSAJE","Visita registrada correctamete");
				}
			}else{
				redirect.addFlashAttribute("ERROR","Visitante aún no ha salido");
				//throw new Exception("Visitante aún no ha salido");
			}
		} catch (Exception e) {
			redirect.addFlashAttribute("ERROR","Error al guardar.");
			e.printStackTrace();
		}
		return "redirect:/visita/";
	}
	
	@RequestMapping("/listacontrolvisitaporparametros")
	@ResponseBody
	public List<ControlVisita> listacontolvisitaporparametros(
			@RequestParam(name = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(name = "dni", required = false, defaultValue = "") String dni,
			@RequestParam(name = "estado", required = false, defaultValue ="1") int estado){
		List<ControlVisita> lista=null;
		try {
			lista=visitaservice.listacontolvisitaporparametros("%"+nombre+"%", dni,estado);
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return lista;	
	}
	
	@RequestMapping("/porDni")
	@ResponseBody
	public Visitante visitantePorDni(@RequestParam("dni") String dni, Model model) {
		Visitante v = null;	
		if(dni.trim().matches("[0-9]{8}")) {
			v = visitanteService.visitantePorDni(dni);
		}
		return v;
	}
	
	
	@RequestMapping("/actualizarVisita")
	public String actualizarVisita(@RequestParam("idActualizaVisita") int id, 
			@RequestParam("salida") String sal,
			@RequestParam("comentario") String com,
			RedirectAttributes redirect) {
		try {
			ControlVisita bean = visitaservice.buscar(id);
			bean.setEstado(0);
			bean.setSalida(new Date());
			bean.setComentario(com);
			visitaservice.registrarControlVisita(bean);
			redirect.addFlashAttribute("MENSAJE","Visita actualizada correctamente");
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("ERROR", "Ocurrio un error al actualizar");
		}
		return "redirect:/visita/";
	}
	
	@RequestMapping(value = "/buscar")
	@ResponseBody
	public ControlVisita buscar(@RequestParam("idVisita") int cod) {
		ControlVisita bean=null;
		try {
			bean=visitaservice.buscar(cod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	

}
