package com.edificio.PID_202201_Grupo5.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.EspecieMascota;
import com.edificio.PID_202201_Grupo5.entity.Mascota;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.entity.Visitante;
import com.edificio.PID_202201_Grupo5.service.DepartamentoServiceImpl;
import com.edificio.PID_202201_Grupo5.service.EspecieMascotaService;
import com.edificio.PID_202201_Grupo5.service.MascotaService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

@Controller
@RequestMapping("/mascota")
public class MascotaController {
	@Autowired
	private EspecieMascotaService especieMascotaService;
	
	@Autowired
	private DepartamentoServiceImpl departamentoService;
	
	@Autowired
	private MascotaService mascotaService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value = "/")
	private String index(Model model, HttpSession session) {
			
		model.addAttribute("especies", especieMascotaService.listarEspecies());
		model.addAttribute("departamentos", departamentoService.listadoDepartamentos());
		model.addAttribute("mascotas", mascotaService.listarMascotaPorEstado(1));
		model.addAttribute("mascotaI", mascotaService.listarMascotaPorEstado(0));
		
		
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());
		
		return "mascota";
	}
	
	@RequestMapping(value = "/guardar")
	public String guardar(@RequestParam("codigo") int cod, @RequestParam("departamento") int dep, @RequestParam("nombre") String nom, 
			@RequestParam("especie") int esp,@RequestParam("estado") int es, RedirectAttributes redirect){
			try {
					Mascota bean= new Mascota();
					bean.setDepartamentoMascota(new Departamento(dep));
					bean.setNombre(nom);
					bean.setEspecie(new EspecieMascota(esp));
					bean.setEstado(1);
					if(cod!=0) {
						bean.setIdmascota(cod);
						mascotaService.actualizar(bean);
						redirect.addFlashAttribute("MENSAJE","Datos de mascota actualizado");
					}
					else {
						mascotaService.registrar(bean);
						redirect.addFlashAttribute("MENSAJE","Se ha registrado la mascota");
					}
								
		} catch (Exception e) {
			redirect.addFlashAttribute("MENSAJE","Error al guardar");
			e.printStackTrace();
		}
		
		return "redirect:/mascota/";
	}
	
	@RequestMapping(value = "/buscar")
	@ResponseBody
	public Mascota buscar(@RequestParam("idMascota") int cod) {
		Mascota bean=null;
		try {
			bean=mascotaService.buscar(cod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	@RequestMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idMascota") int cod,RedirectAttributes redirect) {
		try {
			Mascota m = mascotaService.buscar(cod);
			m.setEstado(0);
			mascotaService.eliminar(m);
			redirect.addFlashAttribute("MENSAJE","Mascota eliminada");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("MENSAJE","Ocurrió un error en la eliminación");
		}
		return "redirect:/mascota/";
	}
	
	@RequestMapping("/activar")
	public String activarMascota(@RequestParam("idMascota") int cod, RedirectAttributes redirect) {
		
		try {
			
			Mascota bean = mascotaService.buscar(cod);
			bean.setEstado(1);
			mascotaService.actualizar(bean);
			redirect.addFlashAttribute("MENSAJE","La mascota ha sido activada");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("error", "Ocurrió un error al activar");
		}
		
		return "redirect:/mascota/";
	}
	
	
	@RequestMapping("/listarmascotasporparametros")
	@ResponseBody
	public  List<Mascota> listarmascotasporparametros(
			@RequestParam(name = "idDepartamento", required = false, defaultValue = "-1") int idDepartamento,
			@RequestParam(name = "idEspecie", required = false, defaultValue = "-1") int especie,
			RedirectAttributes redirect){
			List<Mascota> lista = mascotaService.listarporparametros(idDepartamento, especie);
			redirect.addFlashAttribute("MENSAJE","Se tiene " + lista.size() + " elementos");
			return lista;
	}
	
	
}
