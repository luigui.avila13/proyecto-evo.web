package com.edificio.PID_202201_Grupo5.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.Sexo;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.entity.Visitante;
import com.edificio.PID_202201_Grupo5.service.SexoService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;
import com.edificio.PID_202201_Grupo5.service.VisitanteService;

@Controller
@RequestMapping("/visitante")
public class VisitanteController {
	
	@Autowired
	private SexoService sexoService;
	
	@Autowired
	private VisitanteService visitanteService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value = "/")
	private String index(Model model, HttpSession session) {
			
		model.addAttribute("sexos", sexoService.listarSexo());
		model.addAttribute("visitantes", visitanteService.listarVisitantesPorEstado(1));
		model.addAttribute("visitantesI", visitanteService.listarVisitantesPorEstado(0));		
		
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
		for (var a: lista) {
			Enlace e = new Enlace();
			e.setIdenlace(a.getIdenlace());
			e.setDescripcionEnlace(a.getDescripcionEnlace());
			e.setRuta("../"+a.getRuta());
			e.setListaRolEnlace(null);
			lista2.add(e);
		}
		model.addAttribute("menus", lista2);
		model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());
		
		return "visitante";
	}
	
	@RequestMapping(value = "/guardar")
	public String guardar(@RequestParam("codigo") int cod, @RequestParam("nombre") String nomb, @RequestParam("paterno") String pat, 
			@RequestParam("materno") String mat, @RequestParam("dni") String dni,
			@RequestParam("sexo") int sexo, @RequestParam("estado") int es, RedirectAttributes redirect){
			try {
					Visitante bean=new Visitante();
					bean.setNombres(nomb);
					bean.setApepaterno(pat);
					bean.setApematerno(mat);
					bean.setDni(dni);
					bean.setSexoVisitante(new Sexo(sexo));
					bean.setFecharegistro(new Date());
					bean.setEstado(1);
					if(cod!=0) {
						bean.setIdvisitante(cod);
						visitanteService.actualizar(bean);
						redirect.addFlashAttribute("MENSAJE","Visitante actualizado");
					}
					else {
						visitanteService.registrar(bean);
						redirect.addFlashAttribute("MENSAJE","Visitante registrado");
					}
								
		} catch (Exception e) {
			redirect.addFlashAttribute("MENSAJE","Error al guardar");
			e.printStackTrace();
		}
		
		return "redirect:/visitante/";
	}
	
	@RequestMapping(value = "/buscar")
	@ResponseBody
	public Visitante buscar(@RequestParam("idVisitante") int cod) {
		Visitante bean=null;
		try {
			bean=visitanteService.buscar(cod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	@RequestMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idVisitante") int cod,RedirectAttributes redirect) {
		try {
			Visitante v = visitanteService.buscar(cod);
			v.setEstado(0);
			visitanteService.eliminar(v);
			redirect.addFlashAttribute("MENSAJE","Visitante eliminado");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("MENSAJE","Ocurrio un error en la eliminacion");
		}
		return "redirect:/visitante/";
	}
	
	// Activar
	@RequestMapping("/activar")
	public String activarVisitante(@RequestParam("idVisitante") int id, RedirectAttributes redirect) {
		try {
			Visitante bean = visitanteService.buscar(id);
			bean.setEstado(1);
			visitanteService.actualizar(bean);
			redirect.addFlashAttribute("MENSAJE","Visitante activado correctamente");
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("ERROR", "Ocurrio un error al activar");
		}
		return "redirect:/visitante/";
	}
	
	// Consulta
	@RequestMapping("/PorDNIandSexo")
	@ResponseBody
	public List<Visitante> consulta(
			@RequestParam(value = "dni", required = false, defaultValue = "") String dni, 
			@RequestParam(value = "sexo", required = false, defaultValue = "-1") int sexo,
			@RequestParam(value = "estado", required = true, defaultValue = "1") int estado,
			RedirectAttributes redirect){
		List<Visitante> lista = visitanteService.listaVisitantePorDNIySexo(dni, sexo, estado);
		redirect.addFlashAttribute("MENSAJE","Se tiene " + lista.size() + " elementos");
		return lista;
	}
	
	
	
}
