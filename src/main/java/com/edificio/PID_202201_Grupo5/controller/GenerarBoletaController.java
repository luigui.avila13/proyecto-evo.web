package com.edificio.PID_202201_Grupo5.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.edificio.PID_202201_Grupo5.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.Usuario;

import com.edificio.PID_202201_Grupo5.service.TipoServicioService;
import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.PropietarioService;

import com.edificio.PID_202201_Grupo5.service.BoletaService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/generarBoleta")
public class GenerarBoletaController {
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private TipoServicioService tiposervicioService;

	@Autowired
	private EdificioService edificioService;

	@Autowired
	private DepartamentoService departamentoService;

	@Autowired
	private PropietarioService propietarioService;

	@Autowired
	private BoletaService boletaService;
	

	@RequestMapping("/")
	private String index(HttpSession session, Model model) {

		model.addAttribute("edificio", edificioService.listarActivos(1));
		model.addAttribute("tiposervicio", tiposervicioService.listarTodos());
		
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
		for (var a : lista) {
			Enlace e = new Enlace();
			e.setIdenlace(a.getIdenlace());
			e.setDescripcionEnlace(a.getDescripcionEnlace());
			e.setRuta("../" + a.getRuta());
			e.setListaRolEnlace(null);
			lista2.add(e);
		}
		model.addAttribute("menus", lista2);
		model.addAttribute("fullNameUser", u.getNombres() + " " + u.getApepaterno());
		model.addAttribute("boleta", boletaService.lisatarBoletas());
			return "generarBoleta" ;
	}

	@RequestMapping("/listadepartamentoporedificio")
	@ResponseBody
	public List<Departamento> listaPorEdificio(
			@RequestParam(name = "idEdificio", required = false, defaultValue = "") int idEdificio){
		List<Departamento> lista=null;
		try {
			lista=departamentoService.listarPorEdificio(idEdificio);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	@RequestMapping("/listapropietariopordepartamento")
	@ResponseBody
	public List<PropietarioDep> listaPorDepartamento(
			@RequestParam(name = "idDepartamento", required = false, defaultValue = "") int idDepartamento){
		List<PropietarioDep> lista=null;
		try {
			lista=propietarioService.listarPorDepartamento(idDepartamento);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}





	@RequestMapping(value = "/registrar")
	public String registrar(//@RequestParam("idboleta") int id,
							@RequestParam("servicio") int ser,
							//@RequestParam("fechaemision") String femi,
							@RequestParam("anio") int anio,
							@RequestParam("departamento") int dep,
							@RequestParam("importe") double imp,
							//@RequestParam("estado") int est,
							//@RequestParam("fechapago") Date fpag,
							//@RequestParam("mes") String mes,
							RedirectAttributes redirect){
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			List<Date> lstfecha = listaFechaPago(anio);
			for (Date date : lstfecha) {

				BoletaPorServicio bean=new BoletaPorServicio();
				bean.setServicio(new TipoServicio(ser));
				//bean.setFechaemision(femi);
				bean.setFechaemision(new Date());
				bean.setBoletaDepartamento(new Departamento(dep));
				bean.setImporte(imp);
				bean.setEstadoBoleta(new EstadoBoleta(2));
				bean.setFechapago(date);
				bean.setMes(MesNombre(date.getMonth()));

				boletaService.registrarBoletaPorServicio(bean);

			}
			redirect.addFlashAttribute("MENSAJE","Boletas generadas correctamete.");

		} catch (Exception e) {
			redirect.addFlashAttribute("ERROR","Error al guardar.");
			e.printStackTrace();
		}
		return "redirect:/generarBoleta/";
	}


	@RequestMapping("/listafechasdepagoporanio")
	@ResponseBody
	public static List<Date> listaFechaPago(int anio){
		int[] ultimoDiasMes = {31,28,31,30,31,30,31,31,30,31,30,31};

		//En los años bisiestos el mes de febrero tiene 29 días
		if ((anio % 400 == 0) || (anio % 4 == 0 && anio % 100 != 0)) {
			ultimoDiasMes[1] = 29;
		}

		ArrayList<Date> fechasPago = new ArrayList<Date>();

		Calendar objCalendar = Calendar.getInstance();
		for (int i = 0; i < ultimoDiasMes.length; i++) {
			objCalendar.set(Calendar.YEAR, anio);
			objCalendar.set(Calendar.MONTH, i);
			objCalendar.set(Calendar.DAY_OF_MONTH, ultimoDiasMes[i]);

			//Si es sábado o domingo pagará el viernes, se retrocede unos días
			if (objCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
				objCalendar.set(Calendar.DAY_OF_MONTH, ultimoDiasMes[i]-1);
			}else if (objCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
				objCalendar.set(Calendar.DAY_OF_MONTH, ultimoDiasMes[i]-2);
			}

			Date fechaCambiada =  objCalendar.getTime();
			fechasPago.add(fechaCambiada);
		}

		return fechasPago;
	}

	@RequestMapping("/listafechasdepagoporanioconformato")
	@ResponseBody
	public static List<String> ListaFechaPagoConFormato(int anio) {

		List<String> fechas = new ArrayList<String>();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Date> lstfecha = listaFechaPago(anio);
		for (Date date : lstfecha) {
			//System.out.println(sdf.format(date));
			fechas.add(sdf.format(date));
		}
		return fechas;
	}

	private String MesNombre(int idmes){
		String Mes = "";
		switch (idmes){
			case 0: Mes = "Enero"; break;
			case 1: Mes = "Febrero"; break;
			case 2: Mes = "Marzo"; break;
			case 3: Mes = "Abril"; break;
			case 4: Mes = "Mayo"; break;
			case 5: Mes = "Junio"; break;
			case 6: Mes = "Julio"; break;
			case 7: Mes = "Agosto"; break;
			case 8: Mes = "Setiembre"; break;
			case 9: Mes = "Octubre"; break;
			case 10: Mes = "Noviembre"; break;
			case 11: Mes = "Diciembre"; break;
			default : Mes = ""; break;
		}
		return Mes;
	}

}
