package com.edificio.PID_202201_Grupo5.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.EstadoDepartamento;
import com.edificio.PID_202201_Grupo5.entity.PropietarioDep;
import com.edificio.PID_202201_Grupo5.entity.Sexo;
import com.edificio.PID_202201_Grupo5.entity.TipoDepartamento;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.entity.Visitante;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.EstadoService;
import com.edificio.PID_202201_Grupo5.service.TipoDepartamentoService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;


@Controller
@RequestMapping("/departamento")
public class DepartamentoController {
		
	@Autowired
	private UsuarioService usuarioService;
		
		
	@RequestMapping("/")
	private String index(Model model, HttpSession session) {
		
		model.addAttribute("departamentos", departamentoservice.listarporestado(1));
		model.addAttribute("departamentosInactivos", departamentoservice.listarporestado(0));
			
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());
			
			return "departamento" ;
		}
		
		@Autowired
		private DepartamentoService departamentoservice;
		@Autowired
		private EdificioService edificioservice;
		@Autowired
		private EstadoService estadoservice;
		@Autowired
		private TipoDepartamentoService tipodepartamentoservice;
		
		/*
		@ResponseBody
		@RequestMapping("/listaDepartamento")
		public List<Departamento> listaDepartamento(){
			return departamentoservice.listadoDepartamentos();
		}*/
		
		@ResponseBody
		@RequestMapping("/listaEdificio")
		public List<Edificio> listaEdificio(){
			return edificioservice.listadoEdificio();
		}
		
		@ResponseBody
		@RequestMapping("/listaEstado")
		public List<EstadoDepartamento> listaEstado(){
			return estadoservice.listadoEstado();
		}
		@ResponseBody
		@RequestMapping("/listaTipo")
		public List<TipoDepartamento> listaTipo(){
			return tipodepartamentoservice.listadotipo();
		}
		
		
	/*@RequestMapping("/registraActualizaDepartamento")
		public String insertaActualiza(Departamento departamento, HttpSession session) {
		    try {
		    	departamento.setFecharegistro(new Date());
		        Departamento obj = departamentoservice.insertarDepartamento(departamento);
		        if (obj == null) {
		            session.setAttribute ("MENSAJE", " Error al insertar o actualizar");
		        }else {
		            session.setAttribute ("MENSAJE", "Registro exitoso");
		        }
		     } catch (Exception e) {
		       e.printStackTrace();
		        session.setAttribute ("MENSAJE", "Error al insertar o actualizar");
		     }                                 
		    return "redirect:";
	}*/
		
		
		//Lo Hice en el master profe --- Vilchez 
		@RequestMapping(value = "/guardar")
		public String guardar(@RequestParam("idDepartamento") int cod,
								@RequestParam("edificio")int edificio ,						
								@RequestParam("nrodepartamento")int nrodepartamento,
								@RequestParam("area") String area,
								@RequestParam("tipoDepartamento") int tipoDepartamento,
								@RequestParam("estadoDepartamento") int estadoDepartamento,
							
								@RequestParam("estado") int estado ,RedirectAttributes redirect, HttpSession session)
								
				{
				try {
						Usuario u = (Usuario) session.getAttribute("usuario");
						Departamento bean=new Departamento();
						bean.setEdificio(new Edificio(edificio));
						
						bean.setNrodepartamento(nrodepartamento);
						bean.setArea(area);
						bean.setTipoDepartamento(new TipoDepartamento(tipoDepartamento));
						bean.setEstadoDepartamento(new EstadoDepartamento(estadoDepartamento));
					
						bean.setFecharegistro(new Date());
						bean.setEstado(1);
						bean.setUsuario(new Usuario(u.getIdusuario()));
						if(cod!=0) {
							bean.setIddepartamento(cod);
							departamentoservice.insertarDepartamento(bean);
							redirect.addFlashAttribute("MENSAJE","Departamento actualizado");
						}
						else {
							departamentoservice.insertarDepartamento(bean);
							redirect.addFlashAttribute("MENSAJE","Departamento registrado");
						}
									
			} catch (Exception e) {
				redirect.addFlashAttribute("MENSAJE","Error al guardar");
				e.printStackTrace();
			}
			
			return "redirect:/departamento/";
		}
		
		
		//Lo Hice en el master profe --- Vilchez 
	@RequestMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idDepartamento") int idDepartamento,RedirectAttributes redirect) {
		try {
			Departamento d = departamentoservice.buscaDepartamentoporID(idDepartamento);
			d.setEstado(0);
			departamentoservice.eliminaDepartamento(d);
			
			redirect.addFlashAttribute("MENSAJE","Departamento eliminado");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("MENSAJE","Ocurrio un error al eliminar");
		}
		return "redirect:/departamento/";
		
	}
	
	
	@RequestMapping(value = "/buscar")
	@ResponseBody
	public Departamento buscar(@RequestParam("idDepartamento") int idDepartamento) {
		Departamento bean=null;
		try {
			bean=departamentoservice.buscaDepartamentoporID(idDepartamento);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	@RequestMapping("/activar")
	public String activarEdificio(@RequestParam("idDepartamento") int id, RedirectAttributes redirect) {
		
		try {
			
			Departamento bean = departamentoservice.buscaDepartamentoporID(id);
			bean.setEstado(1);
			departamentoservice.insertarDepartamento(bean);
			redirect.addFlashAttribute("MENSAJE","Edificio activado correctamente");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("error", "Ocurrio un error al activar");
		}
		
		return "redirect:/departamento/";
	}
	
	@RequestMapping("/listaDepartamentoConParametros")
	@ResponseBody
	public  List<Departamento> listadodepartamentosporfiltro(
			@RequestParam(name = "idEdificio", required = false, defaultValue = "-1") int idEdificio,
			@RequestParam(name = "idTipo", required = false, defaultValue = "-1") int idTipo) {
		
		List<Departamento> lista = null;
		try {
			 lista = departamentoservice.listarporparametros(idEdificio,  idTipo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lista ;
		
	}
	

		
}	    
	
