package com.edificio.PID_202201_Grupo5.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.EstadoIncidente;
import com.edificio.PID_202201_Grupo5.entity.Incidente;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.AtencionIncidenteService;
import com.edificio.PID_202201_Grupo5.service.CausaIncidenteService;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.EstadoIncidenteService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

@Controller
@RequestMapping("/atencionIncidente")
public class AtencionIncidenteController {
	
	
	@Autowired 
	private AtencionIncidenteService atencionservice;
	
	@Autowired
	private EdificioService edificioservice;
	
	@Autowired
	private CausaIncidenteService causaservice;
	
	@Autowired
	private EstadoIncidenteService estadoservice;
	
	@Autowired
	private DepartamentoService departamentoService;
	
	@Autowired
	private UsuarioService usuarioService;
	

	@RequestMapping("/")
	private String index(Model model, HttpSession session) {
		
		model.addAttribute("edificio", edificioservice.listadoEdificio());
		model.addAttribute("causa", causaservice.listadotipoincidente());
		model.addAttribute("estado", estadoservice.listarestadoincidente());
		model.addAttribute("departamento", departamentoService.listadoDepartamentos());
		
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());
		
		return "atencionIncidente";
	}
	
	@RequestMapping("/listarincidenteporparametros")
	@ResponseBody
	public List<Incidente> listarincidenteporparametros(
		@RequestParam(name="departamento", required = false,defaultValue = "-1") int departamento,
		@RequestParam(name="causa", required = false,defaultValue = "-1") int causa,
		@RequestParam(name="estado", required = false,defaultValue = "-1") int estado
		){
		List<Incidente> lista=null;
		
		try {
			lista=atencionservice.listaincidenteporparametros(departamento, causa, estado);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
				return lista;
		
	}
	
	@RequestMapping(value ="/buscar")
	@ResponseBody
	public Incidente buscar(@RequestParam("idincidentes")int cod) {
		Incidente bean=null;
		try {
			bean=atencionservice.buscar(cod);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	@RequestMapping("/listadepartamentoporedificio")
	@ResponseBody
	public List<Departamento> listaPorEdificio(
			@RequestParam(name = "idEdificio", required = false, defaultValue = "") int idEdificio){
		List<Departamento> lista=null;
		try {
			lista=departamentoService.listarPorEdificio(idEdificio);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	@RequestMapping("/actualizaratencionincidente")
	public  String acualizaratencion(@RequestParam("actualizar") int id,
									 RedirectAttributes redirect) {
		try {
			Incidente obj = atencionservice.buscar(id);
			obj.setEstadoIncidente(new EstadoIncidente(1,"Atendido" , null));
			atencionservice.registaratencionIncidente(obj);
			redirect.addFlashAttribute("MENSAJE","Incidente actualizado correctamente");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("ERROR", "Ocurrio un error al actualizar");
		}
		return "redirect:/atencionIncidente/";
	}
		
		
		
	
}
