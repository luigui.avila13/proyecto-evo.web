package com.edificio.PID_202201_Grupo5.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.edificio.PID_202201_Grupo5.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.OcupanteService;
import com.edificio.PID_202201_Grupo5.service.RelacionPropietarioService;
import com.edificio.PID_202201_Grupo5.service.SexoService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

@Controller
@RequestMapping("/ocupante")
public class OcupanteController {
	
	@Autowired
	private SexoService sexoService;

	//@Autowired
	//private EdificioService edificioService;

	@Autowired
	private DepartamentoService departamentoService;
	
	@Autowired
	private EdificioService edificioService;
	
	@Autowired
	private RelacionPropietarioService relacionService;
	
	@Autowired 
	private OcupanteService ocupanteService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping("/")
	private String index(Model model, HttpSession session) {

		//model.addAttribute("edificio", edificioService.listarActivos(1));

		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());

			model.addAttribute("sexos", sexoService.listarSexo());
			model.addAttribute("edificio", edificioService.listadoEdificio());
			model.addAttribute("departamentos", departamentoService.listadoDepartamentos());
			model.addAttribute("relaciones", relacionService.listarRelacion());
			model.addAttribute("ocupantes", ocupanteService.listarOcupantesPorEstado(1));
			model.addAttribute("ocupantesInactivos", ocupanteService.listarOcupantesPorEstado(0));
		
		return "ocupante";
	}
	

	@RequestMapping("/listadepartamentoporedificio")
	@ResponseBody
	public List<Departamento> listaPorEdificio(
			@RequestParam(name = "idEdificio", required = false, defaultValue = "") int idEdificio){
		List<Departamento> lista=null;
		try {
			lista=departamentoService.listarPorEdificio(idEdificio);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	@RequestMapping(value = "/guardar")
	public String guardar(@RequestParam("idOcupante") int cod, @RequestParam("departamento") int dep, @RequestParam("edificio") int edi,
			@RequestParam("nombre") String nomb, @RequestParam("paterno") String pat, 
			@RequestParam("materno") String mat, @RequestParam("dni") String dni,
			@RequestParam("celular") String cel, @RequestParam("sexo") int sexo,
			@RequestParam("relacion") int rel, @RequestParam("estado") int es,
			RedirectAttributes redirect){
			try {
					Ocupantes bean=new Ocupantes();
					bean.setEdificioOcupante(new Edificio(edi));
					bean.setDepartamentoOcupante(new Departamento(dep));
					bean.setNombres(nomb);
					bean.setApepaterno(pat);
					bean.setApematerno(mat);
					bean.setDni(dni);
					bean.setCelular(cel);
					bean.setSexoOcupante(new Sexo(sexo));
					bean.setRelacionConPropietario(new RelacionConPropietario(rel));
					bean.setFecharegistro(new Date());
					bean.setEstado(1);
					if(cod!=0) {
						bean.setIdocupante(cod);
						ocupanteService.insertarOcupantes(bean);
						redirect.addFlashAttribute("MENSAJE","Ocupante actualizado.");
					}else{
						ocupanteService.insertarOcupantes(bean);
						redirect.addFlashAttribute("MENSAJE","Ocupante registrado.");
					}
		} catch (Exception e) {
			redirect.addFlashAttribute("MENSAJE","Error al guardar.");
			e.printStackTrace();
		}
		
		return "redirect:/ocupante/";
	}
	
	@RequestMapping("/buscar")
	@ResponseBody
	public Ocupantes buscar(@RequestParam("idOcupante") int cod) {
		Ocupantes bean=null;
		try {
			bean=ocupanteService.buscaOcupantePorID(cod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}

	@RequestMapping("/listarOcupantePorParametros")
	@ResponseBody
	public  List<Ocupantes> listarporparametros(
			@RequestParam(name = "idDepartamento", required = false, defaultValue = "-1") int idDepartamento,
			@RequestParam(name = "idSexo", required = false, defaultValue = "-1") int idSexo,
			@RequestParam(name = "dni", required = false, defaultValue = "%%") String dni) {

		List<Ocupantes> lista = null;
		try {
			lista = ocupanteService.listarPorParametros( idDepartamento, idSexo, dni);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista ;
	}
	
	@RequestMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idOcupante") int cod,RedirectAttributes redirect) {
		try {
			Ocupantes o = ocupanteService.buscaOcupantePorID(cod);
			o.setEstado(0);
			ocupanteService.eliminaOcupante(o);
			redirect.addFlashAttribute("MENSAJE","Ocupante desactivado.");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("MENSAJE","Ocurrió un error en la desactivación.");
		}
		return "redirect:/ocupante/";
	}


	@RequestMapping("/activar")
	public String activar(@RequestParam("idOcupante") int id, RedirectAttributes redirect) {
		try {
			Ocupantes bean = ocupanteService.buscaOcupantePorID(id);
			bean.setEstado(1);
			ocupanteService.insertarOcupantes(bean);
			redirect.addFlashAttribute("MENSAJE","Ocupante activado correctamente.");
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("error", "Ocurrió un error al activar ocupante.");
		}
		return "redirect:/ocupante/";
	}

}
