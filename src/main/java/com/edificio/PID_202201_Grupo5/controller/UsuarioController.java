package com.edificio.PID_202201_Grupo5.controller;


import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;





@Controller
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
		
	@RequestMapping(value = "/login")
	public String iniciarSesion() {
		return "login"; 
	}
	
	@RequestMapping(value = "/")
	public String menu(Authentication auth,Model model, HttpSession session) {
		String usu = auth.getName();
		Usuario user = usuarioService.buscarPorUser(usu);
		session.setAttribute("usuario", user);
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(user.getRolUsuario().getIdrol());
		model.addAttribute("menus", lista);
		
		String fullName = user.getNombres()+" "+user.getApepaterno();
		model.addAttribute("fullNameUser", fullName);
		
		model.addAttribute("rol", user.getRolUsuario().getIdrol());
		model.addAttribute("rolDesc", user.getRolUsuario().getDescripcionrol());
		
		String fullName2 = user.getNombres()+" "+user.getApepaterno()+" "+user.getApematerno();
		model.addAttribute("fullNameUser2", fullName2);
		
		return "index";
	}
	
}
