package com.edificio.PID_202201_Grupo5.controller;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.PropietarioDep;
import com.edificio.PID_202201_Grupo5.entity.Sexo;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.service.DepartamentoService;
import com.edificio.PID_202201_Grupo5.service.EdificioService;
import com.edificio.PID_202201_Grupo5.service.PropietarioService;
import com.edificio.PID_202201_Grupo5.service.SexoService;
import com.edificio.PID_202201_Grupo5.service.UsuarioService;

@Controller
@RequestMapping("/propietario")
public class PropietarioController {
	
	@Autowired
	private PropietarioService propietarioService;
	
	@Autowired
	private SexoService sexoService;

	@Autowired
	private DepartamentoService departamentoService;
	
	@Autowired
	private EdificioService edificioService;
	
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value = "/")
	private String index(Model model, HttpSession session) {
		
		model.addAttribute("propietario", propietarioService.listarPropietariosPorEstado(1));
		model.addAttribute("sexos", sexoService.listarSexo());
		model.addAttribute("edificio", edificioService.listadoEdificio());
		model.addAttribute("departamento", departamentoService.listadoDepartamentos());
		model.addAttribute("propietariosInactivos", propietarioService.listarPropietariosPorEstado(0));
		
		Usuario u = (Usuario) session.getAttribute("usuario");
		List<Enlace> lista = usuarioService.traerEnlacesXUsuario(u.getRolUsuario().getIdrol());
		List<Enlace> lista2 = new ArrayList<Enlace>();
			for (var a: lista) {
				Enlace e = new Enlace();
				e.setIdenlace(a.getIdenlace());
				e.setDescripcionEnlace(a.getDescripcionEnlace());
				e.setRuta("../"+a.getRuta());
				e.setListaRolEnlace(null);
				lista2.add(e);
			}
			model.addAttribute("menus", lista2);
			model.addAttribute("fullNameUser", u.getNombres()+" "+u.getApepaterno());
		
		
		return "propietario";
		
	}
	
	@RequestMapping("/listadepartamentoporedificio")
	@ResponseBody
	public List<Departamento> listaPorEdificio(
			@RequestParam(name = "idEdificio", required = false, defaultValue = "") int idEdificio){
		List<Departamento> lista=null;
		try {
			lista=departamentoService.listarPorEdificio(idEdificio);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}
	
	
	
	@RequestMapping(value = "/guardar")
	public String guardar(@RequestParam("idPropietario")int cod,
			@RequestParam("nombre") String nomb,
			@RequestParam("paterno") String pat, 
			@RequestParam("materno") String mat,
			@RequestParam("dni") String dni,
			@RequestParam("celular") String celular, 
			@RequestParam("correo") String correo,
			@RequestParam("sexo") int sexo,
			@RequestParam("edificio") int edificio, 
			@RequestParam("departamento") int departamento, 
			@RequestParam("estado") int es, RedirectAttributes redirect, HttpSession session) {
		try {
			Usuario u = (Usuario) session.getAttribute("usuario");
			PropietarioDep bean=new PropietarioDep();
			bean.setNombres(nomb);
			bean.setApepaterno(pat);
			bean.setApematerno(mat);
			bean.setDni(dni);
			bean.setCelular(celular);
			bean.setCorreo(correo);
			bean.setSexoPropietario(new Sexo(sexo));
			bean.setEdificioPropietario(new Edificio(edificio));
			bean.setDepartamentoPropietario(new Departamento(departamento));
			bean.setFecharegistro(new Date());
			bean.setEstado(1);
			bean.setUsuario2(new Usuario(u.getIdusuario()));
			
			if(cod!=0) {
				bean.setIdpropietario(cod);
				propietarioService.actualizar(bean);
				redirect.addFlashAttribute("MENSAJE","Propietario actualizado");
			}
			else {
				propietarioService.registrar(bean);
				redirect.addFlashAttribute("MENSAJE","Propietario registrado");
			}
			
		} catch (Exception e) {
			
			redirect.addFlashAttribute("MENSAJE","Error al guardar");
			e.printStackTrace();
		}
		return "redirect:/propietario/";
		
	}
	@RequestMapping(value = "/buscar")
	@ResponseBody
	public PropietarioDep buscar(@RequestParam("idPropietario") int cod) {
		PropietarioDep bean=null;
		try {
			bean=propietarioService.buscar(cod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	@RequestMapping(value = "/eliminar")
	public String eliminar(@RequestParam("idPropietario") int cod,RedirectAttributes redirect) {
		try {
			PropietarioDep p = propietarioService.buscar(cod);
			p.setEstado(0);
			propietarioService.eliminar(p);
			redirect.addFlashAttribute("MENSAJE","Propietario eliminado");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("MENSAJE","Ocurrio un error en la eliminacion");
		}
		return "redirect:/propietario/";
	}
	
	@RequestMapping("/activar")
	public String activarPropietario(@RequestParam("idPropietario") int id, RedirectAttributes redirect) {
		
		try {
			
			PropietarioDep bean = propietarioService.buscar(id);
			bean.setEstado(1);
			propietarioService.registrar(bean);
			redirect.addFlashAttribute("MENSAJE", "Propietario activado");
			
		} catch (Exception e) {
			e.printStackTrace();
			redirect.addFlashAttribute("MENSAJE","Ocurrio un error al activar");
			
		}
		return "redirect:/propietario/";
		
	}
	@RequestMapping("/listarPropietariosConParametros")
	@ResponseBody
	public List<PropietarioDep> listadopropietarioporfiltro(
			@RequestParam(name = "nombre", required = false, defaultValue = "") String nombres,
			@RequestParam(name ="dni", required = false, defaultValue =  "") String dni){
			
			List<PropietarioDep> lista = null;
			try {
				lista = propietarioService.listarPorParametros("%"+nombres +"%", dni);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return lista;
	}
	
}

