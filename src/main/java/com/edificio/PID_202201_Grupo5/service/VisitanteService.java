package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Visitante;
import com.edificio.PID_202201_Grupo5.repository.VisitanteRepository;

@Service
public class VisitanteService {

	@Autowired
	private VisitanteRepository visitanteRepository;
	
	public void registrar(Visitante bean) {
		visitanteRepository.save(bean);
	}
	
	public void actualizar(Visitante bean) {
		visitanteRepository.save(bean);
	}
	
	public void eliminar(Visitante bean) {
		visitanteRepository.save(bean);
	}
	
	public Visitante buscar(int cod) {
		return visitanteRepository.findById(cod).orElse(null);
	}
	
	public List<Visitante> listarTodos(){
		return visitanteRepository.findAll();
	}	
	
	public List<Visitante> listarVisitantesPorEstado(int estado){
		return visitanteRepository.findAllByEstado(estado);
	}	
	
	// Consulta
	public List<Visitante> listaVisitantePorDNIySexo(String dni, int sexo, int estado){
		return visitanteRepository.listaVisitantePorDNIandSexo(dni, sexo, estado);
	}
	
	
	public Visitante visitantePorDni(String dni) {
		return visitanteRepository.findByDni(dni);
	}
	
	
}
