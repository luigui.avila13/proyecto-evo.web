package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.BoletaPorServicio;
import com.edificio.PID_202201_Grupo5.entity.ControlVisita;

public interface BoletaService {

	public abstract BoletaPorServicio registrarBoletaPorServicio(BoletaPorServicio obj);
	
	public abstract List<BoletaPorServicio> lisatarBoletas();
	
	public abstract List<BoletaPorServicio> filtroPorParametrosBoleta(int nroDepa,String mes, int servicio, int estado);
	
	public abstract BoletaPorServicio procesoDePago(BoletaPorServicio obj);
	
	public abstract BoletaPorServicio buscarPorId(int id);
}
