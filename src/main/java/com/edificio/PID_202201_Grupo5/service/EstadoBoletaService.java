package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.EstadoBoleta;

public interface EstadoBoletaService {
	
	public abstract List<EstadoBoleta> listarTodos();
}
