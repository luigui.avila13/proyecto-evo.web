package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.TipoDepartamento;

public interface TipoDepartamentoService {
	public abstract List<TipoDepartamento> listadotipo();
}
