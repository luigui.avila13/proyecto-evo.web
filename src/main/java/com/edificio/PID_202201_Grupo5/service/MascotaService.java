package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Mascota;
import com.edificio.PID_202201_Grupo5.entity.Visitante;
import com.edificio.PID_202201_Grupo5.repository.MascotaRepository;

@Service
public class MascotaService {

	
	@Autowired
	private MascotaRepository mascotaRepository;
	
	public void registrar(Mascota bean) {
		mascotaRepository.save(bean);
	}
	
	public void actualizar(Mascota bean) {
		mascotaRepository.save(bean);
	}
	
	public void eliminar(Mascota bean) {
		mascotaRepository.save(bean);
	}
	
	public Mascota buscar(int cod) {
		return mascotaRepository.findById(cod).orElse(null);
	}
	
	public List<Mascota> listar(){
		return mascotaRepository.findAll();
	}
	
	
	public List<Mascota> listarMascotaPorEstado(int estado){
		return mascotaRepository.findAllByEstado(estado);
	}
	//
	
	public List<Mascota> listarporparametros(int idDepartamento, int especie){
		return mascotaRepository.listarporparametros(idDepartamento, especie);
	}
	
}
