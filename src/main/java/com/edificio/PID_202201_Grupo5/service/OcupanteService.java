package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.Ocupantes;

public interface OcupanteService {
	public abstract List<Ocupantes> listarTodos();
	public abstract Ocupantes insertarOcupantes(Ocupantes obj);
	public abstract void eliminaOcupante(Ocupantes bean);
	public abstract Ocupantes buscaOcupantePorID(int idOcupante);
	public abstract List<Ocupantes> listarOcupantesPorEstado(int estado);
	public abstract List<Ocupantes> listarPorParametros(int idDepartamento, int idSexo, String dni);
	
}