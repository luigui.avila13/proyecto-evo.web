package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Ocupantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.ControlVisita;
import com.edificio.PID_202201_Grupo5.repository.ControlVisitaRepository;

import javax.naming.ldap.Control;

@Service
public class ControlVisitaServiceImpl implements ControlVisitaService {
	
	@Autowired
	private ControlVisitaRepository repository;

	@Override
	public ControlVisita registrarControlVisita(ControlVisita bean) {
		return repository.save(bean);
	}

	@Override
	public List<ControlVisita> listacontolvisitaporparametros(String nombre, String dni, int estado) {
		// TODO Auto-generated method stub
		return repository.listacontolvisitaporparametros(nombre, dni,estado);
	}

	@Override
	public List<ControlVisita> listarporestado(int estado) {
		// TODO Auto-generated method stub
		return repository.findAllByEstado(estado);
	}

	@Override
	public ControlVisita buscar(int cod) {
		// TODO Auto-generated method stub
		return repository.findById(cod).orElse(null);
	}

}
