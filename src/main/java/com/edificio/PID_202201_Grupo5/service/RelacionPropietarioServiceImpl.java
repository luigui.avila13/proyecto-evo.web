package com.edificio.PID_202201_Grupo5.service;

import com.edificio.PID_202201_Grupo5.entity.RelacionConPropietario;
import com.edificio.PID_202201_Grupo5.repository.RelacionPropietarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelacionPropietarioServiceImpl implements RelacionPropietarioService {

    @Autowired
    private RelacionPropietarioRepository repository;

    @Override
    public List<RelacionConPropietario> listarRelacion() {
        return repository.findAll();
    }
}
