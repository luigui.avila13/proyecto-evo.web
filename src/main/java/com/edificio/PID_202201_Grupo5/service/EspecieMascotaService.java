package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.EspecieMascota;
import com.edificio.PID_202201_Grupo5.repository.EspecieMascotaRepository;

@Service
public class EspecieMascotaService {

	@Autowired
	private EspecieMascotaRepository especieMascotaRepository;
	
	public List<EspecieMascota> listarEspecies(){
		return especieMascotaRepository.findAll();
	}
}
