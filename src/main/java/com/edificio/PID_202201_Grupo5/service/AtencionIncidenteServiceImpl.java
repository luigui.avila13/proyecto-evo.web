package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Incidente;
import com.edificio.PID_202201_Grupo5.repository.AtencionIndicenteRepository;

@Service
public class AtencionIncidenteServiceImpl implements AtencionIncidenteService{

	@Autowired
	private AtencionIndicenteRepository repo;
	
	@Override
	public List<Incidente> listaincidenteporparametros(int departamento, int tipoincidente, int estado) {
		// TODO Auto-generated method stub
		return repo.listaincidenteporparametros(departamento, tipoincidente, estado);
	}

	@Override
	public Incidente buscar(int cod) {
		// TODO Auto-generated method stub
		return repo.findById(cod).orElse(null);
	}

	@Override
	public Incidente registaratencionIncidente(Incidente obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	

}
