package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Sexo;
import com.edificio.PID_202201_Grupo5.repository.SexoRepository;

@Service
public class SexoService {
	
	@Autowired
	private SexoRepository sexoRepository;
	
	public List<Sexo> listarSexo(){
		return sexoRepository.findAll();
	}
}
