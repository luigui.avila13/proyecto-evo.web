package com.edificio.PID_202201_Grupo5.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Incidente;
import com.edificio.PID_202201_Grupo5.repository.IncidenteRepository;

@Service
public class IncidenteService {
	
	@Autowired
	private IncidenteRepository incidenteRepository;
	
	public void registrar(Incidente bean) {
		incidenteRepository.save(bean);
	}

}
