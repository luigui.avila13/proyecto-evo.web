package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.RelacionConPropietario;

public interface RelacionPropietarioService {
	public abstract List<RelacionConPropietario> listarRelacion();

	
}