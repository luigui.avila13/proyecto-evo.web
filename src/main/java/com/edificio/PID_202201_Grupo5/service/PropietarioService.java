package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.PropietarioDep;
import com.edificio.PID_202201_Grupo5.repository.PropietarioRepository;

@Service
public class PropietarioService {

	@Autowired
	private PropietarioRepository propietarioRepository;
	
	public void registrar(PropietarioDep bean) {
		propietarioRepository.save(bean);
	}
	
	public void actualizar(PropietarioDep bean) {
		propietarioRepository.save(bean);
	}
	
	public void eliminar(PropietarioDep bean) {
		propietarioRepository.save(bean);
	}
	
	public PropietarioDep buscar(int cod) {
		return propietarioRepository.findById(cod).orElse(null);
	}
	
	public List<PropietarioDep> listarTodos(){
		return propietarioRepository.findAll();
	}
	
	public List<PropietarioDep> listarPropietariosPorEstado(int estado){
		return propietarioRepository.findAllByEstado(estado);
	}
	public List<PropietarioDep> listarPorParametros(String nombres, String dni){
		return propietarioRepository.listarPorParametros(nombres, dni);
	}

	public List<PropietarioDep> listarPorDepartamento(int idDepartamento){
		return propietarioRepository.listarPorDepartamento(idDepartamento);
	};
	
	public List<PropietarioDep> listarPorEdificio(int idEdificio){
		return propietarioRepository.listarPorEdificio(idEdificio);
	};

}

