package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.Usuario;

public interface UsuarioService {

	public abstract Usuario buscarPorUser(String user);
	public abstract List<Enlace> traerEnlacesXUsuario(int idRol);
}
