package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.TipoIncidente;
import com.edificio.PID_202201_Grupo5.repository.TipoIncidenteRepository;

@Service
public class TipoIncidenteService {
	
	@Autowired
	private TipoIncidenteRepository tipoIncidenteRepository;

	public List<TipoIncidente> listarTipoIncidentes(){
		return tipoIncidenteRepository.findAll();
	}
	
}
