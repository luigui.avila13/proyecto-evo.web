package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Edificio;

public interface EdificioService {
	public abstract List<Edificio> listadoEdificio();
	public abstract Edificio registrarEdificio(Edificio e);
	public abstract Edificio actualizarEdificio(Edificio eActualizado);
	public abstract List<Edificio> listarActivos(int estado);
	public abstract Edificio buscarPorCodigo(int codEdificio);
	
}
