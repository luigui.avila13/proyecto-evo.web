package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.Edificio;

public interface DepartamentoService {
	
	public abstract List<Departamento> listadoDepartamentos();
	public abstract Departamento insertarDepartamento(Departamento obj);
	public abstract void eliminaDepartamento(Departamento bean);
	public abstract Departamento buscaDepartamentoporID(int idDepartamento);
	public abstract List<Departamento> listarporestado(int estado);
	public abstract List<Departamento> listarPorEdificio(int idEdificio);
	public abstract List<Departamento> listarporparametros(int idEdificio, int idTipo);
	
	
}