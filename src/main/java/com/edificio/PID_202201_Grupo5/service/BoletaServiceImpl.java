package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.ControlVisita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.BoletaPorServicio;
import com.edificio.PID_202201_Grupo5.repository.BoletaRepository;

@Service
public class BoletaServiceImpl implements BoletaService{

	@Autowired
	private BoletaRepository repository;

	@Override
	public BoletaPorServicio registrarBoletaPorServicio(BoletaPorServicio bean) {
		return repository.save(bean);
	}
	
	@Override
	public List<BoletaPorServicio> filtroPorParametrosBoleta(int nroDepa,String mes, int servicio, int estado) {
		return repository.listarBoletasPorParametros(nroDepa, mes, servicio, estado);
	}

	@Override
	public List<BoletaPorServicio> lisatarBoletas() {
		
		return repository.findAll();
	}
	
	@Override
	public BoletaPorServicio procesoDePago(BoletaPorServicio obj) {
		return repository.save(obj);
	}

	@Override
	public BoletaPorServicio buscarPorId(int id) {
		return repository.findById(id).orElse(null);
	}
	
	

}
