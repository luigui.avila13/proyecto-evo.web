package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.TipoServicio;

public interface TipoServicioService {
	
	public abstract List<TipoServicio> listarTodos();
}
