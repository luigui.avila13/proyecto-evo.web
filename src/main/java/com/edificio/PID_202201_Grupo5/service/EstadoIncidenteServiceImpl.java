package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.EstadoIncidente;
import com.edificio.PID_202201_Grupo5.repository.EstadoIncidenteRepository;

@Service
public class EstadoIncidenteServiceImpl implements EstadoIncidenteService{
	
	@Autowired
	private EstadoIncidenteRepository repo;

	@Override
	public List<EstadoIncidente> listarestadoincidente() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
	

}
