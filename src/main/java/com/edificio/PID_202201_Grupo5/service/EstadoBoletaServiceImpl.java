package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.EstadoBoleta;
import com.edificio.PID_202201_Grupo5.repository.EstadoBoletaRepository;

@Service
public class EstadoBoletaServiceImpl implements EstadoBoletaService{

	@Autowired
	private EstadoBoletaRepository repository;
	
	@Override
	public List<EstadoBoleta> listarTodos() {
		return repository.findAll();
	}

}
