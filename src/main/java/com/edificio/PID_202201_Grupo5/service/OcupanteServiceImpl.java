package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Ocupantes;
import com.edificio.PID_202201_Grupo5.repository.OcupanteRepository;

@Service
public class OcupanteServiceImpl implements OcupanteService {

    @Autowired
    private OcupanteRepository repository;

    @Override
    public Ocupantes insertarOcupantes(Ocupantes bean) {
        return repository.save(bean);
    }

	@Override
	public void eliminaOcupante(Ocupantes bean) {
		repository.save(bean);
	}

    @Override
    public Ocupantes buscaOcupantePorID(int idOcupante) {
        return repository.findById(idOcupante).orElse(null);
    }

    @Override
    public List<Ocupantes> listarTodos() {
        return repository.findAll();
    }

	@Override
	public List<Ocupantes> listarOcupantesPorEstado(int estado) {
		return repository.findAllByEstado(estado);
	}

    @Override
    public List<Ocupantes> listarPorParametros( int idDepartamento, int idSexo, String dni) {
        return repository.listarPorParametros(idDepartamento, idSexo, dni);
    }
}
