package com.edificio.PID_202201_Grupo5.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.repository.DepartamentoRepository;

@Service
public class DepartamentoServiceImpl implements DepartamentoService{

	@Autowired
	private DepartamentoRepository repository;
	
	
	@Override
	public List<Departamento> listadoDepartamentos() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Departamento insertarDepartamento(Departamento obj) {
		// TODO Auto-generated method stub
		return repository.save(obj);
	}
	
	

	@Override
	public void eliminaDepartamento(Departamento bean) {
		repository.save(bean);
		
	}

	@Override
	public Departamento buscaDepartamentoporID(int idDepartamento) {
		 return repository.findById(idDepartamento).orElse(null);
	}

	@Override
	public List<Departamento> listarporestado(int estado) {
		return repository.findAllByEstado(estado);
	}

	public List<Departamento> listarPorEdificio(int idEdificio) {
		// TODO Auto-generated method stub
		return repository.listarPorEdificio(idEdificio);
	}

	@Override
	public List<Departamento> listarporparametros(int idEdificio, int idTipo) {
		// TODO Auto-generated method stub
		return repository.listarporparametros(idEdificio,  idTipo);
	}
}
