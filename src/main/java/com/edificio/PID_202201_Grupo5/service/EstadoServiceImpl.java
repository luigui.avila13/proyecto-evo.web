package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.EstadoDepartamento;
import com.edificio.PID_202201_Grupo5.repository.EstadoRepository;

@Service
public class EstadoServiceImpl implements EstadoService {
@Autowired
private EstadoRepository repository;

@Override
public List<EstadoDepartamento> listadoEstado() {
	// TODO Auto-generated method stub
	return repository.findAll();
}

}
