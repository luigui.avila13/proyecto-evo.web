package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.TipoIncidente;

public interface CausaIncidenteService {

	public abstract List<TipoIncidente> listadotipoincidente();
}
