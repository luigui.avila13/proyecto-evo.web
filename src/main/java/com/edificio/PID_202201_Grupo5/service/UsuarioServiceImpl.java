package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Enlace;
import com.edificio.PID_202201_Grupo5.entity.Usuario;
import com.edificio.PID_202201_Grupo5.repository.UsuarioRepository;



@Service
public class UsuarioServiceImpl  implements UsuarioService{

	@Autowired
	private UsuarioRepository repository;

	@Override
	public Usuario buscarPorUser(String user) {
		return repository.findByUser(user);
	}

	@Override
	public List<Enlace> traerEnlacesXUsuario(int idRol) {
		return repository.traearEnlaces(idRol);
	}
	
	
}
