package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.TipoIncidente;
import com.edificio.PID_202201_Grupo5.repository.CausaIncideteRepsitory;

@Service
public class CausaIncidenteServiceImp implements CausaIncidenteService{

	@Autowired
	private CausaIncideteRepsitory repo;

	@Override
	public List<TipoIncidente> listadotipoincidente() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
	
	
}
