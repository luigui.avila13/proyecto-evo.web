package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.ControlVisita;

public interface ControlVisitaService {

	public abstract ControlVisita registrarControlVisita(ControlVisita obj);
	public abstract List<ControlVisita> listacontolvisitaporparametros(String nombre, String dni,int estado);
	public abstract List<ControlVisita> listarporestado(int estado);
	public ControlVisita buscar(int cod);

}
