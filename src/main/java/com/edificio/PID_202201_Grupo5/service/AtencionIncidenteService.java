package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.entity.Incidente;

public interface AtencionIncidenteService {
	
	public List<Incidente> listaincidenteporparametros( int departamento, int tipoincidente, int estado );
	public Incidente buscar(int cod);
	public abstract Incidente registaratencionIncidente(Incidente obj);
	
	

}
