package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.Edificio;
import com.edificio.PID_202201_Grupo5.repository.edificioRepository;

@Service
public class EdificioServiceImpl implements EdificioService{

	@Autowired
	private edificioRepository repository;
	
	@Override
	public List<Edificio> listadoEdificio() {
		return repository.findAll();
	}

	@Override
	public Edificio registrarEdificio(Edificio e) {
		return repository.save(e);
	}

	@Override
	public Edificio actualizarEdificio(Edificio eActualizado) {
		return repository.save(eActualizado);
	}

	
	@Override
	public List<Edificio> listarActivos(int estado) {
		return repository.findAllByEstado(estado);
	}

	@Override
	public Edificio buscarPorCodigo(int codEdificio) {
		return repository.findById(codEdificio).orElse(null);
	}

}
