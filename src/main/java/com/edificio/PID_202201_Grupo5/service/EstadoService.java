package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import com.edificio.PID_202201_Grupo5.entity.Departamento;
import com.edificio.PID_202201_Grupo5.entity.EstadoDepartamento;

public interface EstadoService {
	public abstract List<EstadoDepartamento> listadoEstado();

}
