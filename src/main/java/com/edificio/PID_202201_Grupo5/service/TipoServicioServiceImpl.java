package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.TipoServicio;
import com.edificio.PID_202201_Grupo5.repository.TipoServicioRepository;

@Service
public class TipoServicioServiceImpl implements TipoServicioService{

	@Autowired
	private TipoServicioRepository repository;
	
	@Override
	public List<TipoServicio> listarTodos() {
		return repository.findAll();
	}

}
