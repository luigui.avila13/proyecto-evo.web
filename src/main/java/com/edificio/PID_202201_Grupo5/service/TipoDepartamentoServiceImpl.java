package com.edificio.PID_202201_Grupo5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edificio.PID_202201_Grupo5.entity.TipoDepartamento;
import com.edificio.PID_202201_Grupo5.repository.TipoDepartamentoRepository;

@Service
public class TipoDepartamentoServiceImpl implements TipoDepartamentoService{

	@Autowired
	private TipoDepartamentoRepository repository;

	@Override
	public List<TipoDepartamento> listadotipo() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}
	
}
