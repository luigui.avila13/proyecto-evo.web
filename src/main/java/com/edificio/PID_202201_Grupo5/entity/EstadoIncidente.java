package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "estadoincidente")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoIncidente implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idestadoincidente;
	
	@Column(name = "descripcion")
	private String descripcionEstadoIncidente;
	
	@OneToMany(mappedBy = "estadoIncidente")
	@JsonIgnore
	private List<Incidente> listaIncidente;

	public EstadoIncidente(int idestadoincidente) {
		super();
		this.idestadoincidente = idestadoincidente;
	}
	
}
