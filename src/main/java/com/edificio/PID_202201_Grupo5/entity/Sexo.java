package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "sexo")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Sexo implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idsexo;
	
	@Column(name = "descripcion")
	private String descripcionSexo;
	
	@JsonIgnore
	@OneToMany(mappedBy = "sexoPropietario")
	private List<PropietarioDep> listaPropietario;
	
	@JsonIgnore
	@OneToMany(mappedBy = "sexoVisitante")
	private List<Visitante> listaVisitante;
	
	@JsonIgnore
	@OneToMany(mappedBy = "sexoOcupante")
	private List<Ocupantes> listaOcupante;

	public Sexo(int idsexo) {
		super();
		this.idsexo = idsexo;
	}	
	
}
