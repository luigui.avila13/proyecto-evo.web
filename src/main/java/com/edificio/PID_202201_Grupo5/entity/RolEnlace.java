package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "rol_enlace")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RolEnlace implements Serializable{

	@EmbeddedId
	private RolEnlacePK id;
	
	@ManyToOne
	@JoinColumn(name = "idrol", referencedColumnName = "idrol", insertable = false, updatable = false )
	private Rol rol;
	
	@ManyToOne
	@JoinColumn(name = "idenlace", referencedColumnName = "idenlace", insertable = false, updatable = false)
	private Enlace enlace;
	
}
