package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "incidentes")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Incidente implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idincidentes;
	
	@ManyToOne
	@JoinColumn(name = "departamentoFK")
	private Departamento incidenteDepartamento;
	
	
	@ManyToOne
	@JoinColumn(name = "usuarioFK")
	@JsonIgnore
	private Usuario usuarioIncidente;
	
	@ManyToOne
	@JoinColumn(name = "tipoincidenteFK")
	private TipoIncidente incidente;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date fechaincidente;
	
	@ManyToOne
	@JoinColumn(name = "estadoFK")
	private EstadoIncidente estadoIncidente;
	
	private String comentario;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecharegistro;
	
}
