package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "propietariodep")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PropietarioDep implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idpropietario;

	private String nombres;
	
	private String apepaterno;
	
	private String apematerno;
	
	private String dni;
	
	private String celular;
	
	private String correo;
	
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "autorregistro")
	private Usuario usuario2;
	
	@ManyToOne
	@JoinColumn(name = "sexoFK")
	private Sexo sexoPropietario;
		
	
	@ManyToOne
	@JoinColumn(name = "departamentoFK")
	private Departamento departamentoPropietario;
	
	@ManyToOne
	@JoinColumn(name = "edificioFK2")
	private Edificio edificioPropietario;
	
	
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Lima" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecharegistro;

	private int estado;


}
