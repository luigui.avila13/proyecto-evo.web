package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tiposervicio")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipoServicio implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idtiposervicio;
	
	@Column(name = "descripcion")
	private String descripcionServicio;
	
	@JsonIgnore
	@OneToMany(mappedBy = "servicio")
	private List<BoletaPorServicio> listaBoletaPorServicio;

	public TipoServicio(int idtiposervicio) {
		super();
		this.idtiposervicio = idtiposervicio;
	}
}
