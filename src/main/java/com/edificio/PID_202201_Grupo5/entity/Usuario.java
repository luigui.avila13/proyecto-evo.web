package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="usuario")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Usuario implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idusuario;

	private String nombres;
	
	private String apepaterno;
	
	private String apematerno;
	
	private String user;
	
	private String password;
	
	@ManyToOne
	@JoinColumn(name = "idrol")
	private Rol rolUsuario;
	
	@JsonIgnore
	@OneToMany(mappedBy = "usuarioIncidente")
	private List<Incidente> listaIncidente;
	
	@JsonIgnore
	@OneToMany(mappedBy = "usuario")
	private List<Departamento> listaDepartamento;
	
	@JsonIgnore
	@OneToMany(mappedBy = "usuario2")
	private List<PropietarioDep> listaPropietario;

	public Usuario(int idusuario) {
		super();
		this.idusuario = idusuario;
	}
	
	
	
}
