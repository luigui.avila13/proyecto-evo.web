package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "controlvisita")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ControlVisita implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idvisita;
	
	@ManyToOne
	@JoinColumn(name = "visitanteFK")
	private Visitante visitanteVisita;
	
	@ManyToOne
	@JoinColumn(name = "departamentoFK")
	private Departamento departamentoVisita;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Lima" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date ingreso;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Lima" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date salida;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date fecharegistro;
	
	private int estado;
	
	private String comentario;
	
}
