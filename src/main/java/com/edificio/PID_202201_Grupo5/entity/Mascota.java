package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "mascotas")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Mascota implements Serializable{
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idmascota;
	
	@ManyToOne
	@JoinColumn(name = "departamentoFK")
	private Departamento departamentoMascota;
	
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name = "especieFK")
	private EspecieMascota especie;
	
	private int estado;
}
