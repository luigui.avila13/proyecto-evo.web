package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "visitante")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Visitante implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idvisitante;
	
	private String nombres;
	
	private String apepaterno;
	
	private String apematerno;
	
	private String dni;
	
	@ManyToOne
	@JoinColumn(name = "sexoFK")
	private Sexo sexoVisitante;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Lima" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecharegistro;
	
	private int estado;
	
	@JsonIgnore
	@OneToMany(mappedBy = "visitanteVisita")
	private List<ControlVisita> listaControlVisita;

	public Visitante(int idvisitante) {
		super();
		this.idvisitante = idvisitante;
	}
}
