package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ocupantes")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Ocupantes implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idocupante;
	
	@ManyToOne
	@JoinColumn(name = "departamentoFK")
	private Departamento departamentoOcupante;
	
	@ManyToOne
	@JoinColumn(name = "edificioFK2")
	private Edificio edificioOcupante;
	
	
	
	private String nombres;
	
	private String apepaterno;
	
	private String apematerno;
	
	private String dni;
	
	private String celular;
	
	@ManyToOne
	@JoinColumn(name = "sexoFK")
	private Sexo sexoOcupante;
	
	@ManyToOne
	@JoinColumn(name = "relacionFK")
	private RelacionConPropietario relacionConPropietario;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecharegistro;
	
	private int estado;

}
