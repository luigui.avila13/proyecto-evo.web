package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "enlace")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Enlace implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idenlace;
	
	@Column(name = "descripcion")
	private String descripcionEnlace;
	
	@Column(name = "ruta")
	private String ruta;
	
	@OneToMany(mappedBy = "enlace")
	private List<RolEnlace> listaRolEnlace;
}
