package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "estadoboleta")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EstadoBoleta implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idestadoboleta;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@JsonIgnore
	@OneToMany(mappedBy = "estadoBoleta")
	private List<BoletaPorServicio> listaBoletaPorServicio;

	public EstadoBoleta(int idestadoboleta) {
		super();
		this.idestadoboleta = idestadoboleta;
	}
	
}
