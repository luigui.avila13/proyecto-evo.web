package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "boletaxservicio")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BoletaPorServicio implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idboleta;
	
	@ManyToOne
	@JoinColumn(name = "tiposervicioFK")
	private TipoServicio servicio;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date fechaemision;
	
	@ManyToOne
	@JoinColumn(name = "departamentoFK")
	private Departamento boletaDepartamento;
	
	private double importe;
	
	@ManyToOne
	@JoinColumn(name = "estadoFK")
	private EstadoBoleta estadoBoleta;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date fechapago;
	
	private String mes;
	
}
