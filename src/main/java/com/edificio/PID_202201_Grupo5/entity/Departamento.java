package com.edificio.PID_202201_Grupo5.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "departamento")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Departamento implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iddepartamento;
	
	
	

	
	
	private int nrodepartamento;
	
	@Column(name="areaM2")
	private String area;
	
	private int estado;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "autorregistro")
	private Usuario usuario;



	
			
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecharegistro;
	

	@JsonIgnore
	@OneToMany(mappedBy = "departamentoPropietario")
	private List<PropietarioDep> listaPropietario;
	
	@JsonIgnore
	@OneToMany(mappedBy = "departamentoOcupante")
	private List<Ocupantes> listaOcupante;
	
	@JsonIgnore
	@OneToMany(mappedBy = "departamentoMascota")
	private List<Mascota> listaMascota;
	
	@JsonIgnore
	@OneToMany(mappedBy = "incidenteDepartamento")
	private List<Incidente> listaIncidente;
	
	@JsonIgnore
	@OneToMany(mappedBy = "boletaDepartamento")
	private List<BoletaPorServicio> listaBoletaPorServicio;

	@JsonIgnore
	@OneToMany(mappedBy = "departamentoVisita")
	private List<ControlVisita> listaControlVisita;
	
	@ManyToOne
	@JoinColumn(name = "tipodepartamentoFK")
	private TipoDepartamento tipoDepartamento;
	
	@ManyToOne
	@JoinColumn(name = "estadoFK")
	private EstadoDepartamento estadoDepartamento;
	
	
	@ManyToOne
	@JoinColumn(name = "edificioFK")
	private Edificio edificio;



	public Departamento(int iddepartamento) {
		super();
		this.iddepartamento = iddepartamento;
	}
	
	

}

